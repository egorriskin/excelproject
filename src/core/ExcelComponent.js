import { DomListener } from '@core/DomListener';

export class ExcelComponent extends DomListener {
  constructor($root, options = {}) {
    super($root, options.listeners);
    this.name = options.name || '';
    this.emitter = options.emitter;
    this.unsubscribers = [];

    this.prepare();
  }

  // настройка компонента до init
  prepare() {}

  // Возвращает шаблон компонента
  toHtml() {
    return '';
  }

  // уведомление слушателей о событии event
  $emit(event, ...args) {
    this.emitter.emit(event, ...args);
  }

  // подписка на событие event
  $on(event, fn) {
    const unsub = this.emitter.subscribe(event, fn);
    this.unsubscribers.push(unsub);
  }

  // инициализация компонента,
  // добавление ДОМ слушателей
  init() {
    this.initDomListeners();
  }

  // удаление компонента, чистка слушателей
  destroy() {
    this.removeDomListeners();
    this.unsubscribers.forEach((unsub) => unsub());
  }
}
